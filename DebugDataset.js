/*global
YAHOO
*/

(function () {
    YAHOO.namespace("support.v1_0_0");
    var application = YAHOO.support.v1_0_0;
    var Toronto = YAHOO.com.aviarc.framework.toronto;

    application.DebugDataset = function () {
        
    };

    YAHOO.lang.extend(application.DebugDataset, Toronto.framework.DefaultActionImpl, {
        run: function (state) {
            // Add the action implementation here
        }
    });
})();
