
(function() {

    YAHOO.namespace('com.aviarc.framework.toronto.internal');
    /* This exposes 'Toronto' at the top level for the whole page */
    var Toronto = YAHOO.com.aviarc.framework.toronto;
    var COMMIT_COLOR = {};

    COMMIT_COLOR[Toronto.core.DatasetRow.COMMIT_NONE] = '%c{color:black}none';
    COMMIT_COLOR[Toronto.core.DatasetRow.COMMIT_CREATE] = '%c{color:green;font-weight:bold}create';
    COMMIT_COLOR[Toronto.core.DatasetRow.COMMIT_DELETE] = '%c{color:red;font-weight:bold}delete';
    COMMIT_COLOR[Toronto.core.DatasetRow.COMMIT_UPDATE] = '%c{color:orange;font-weight:bold}update';
    COMMIT_COLOR[Toronto.core.DatasetRow.COMMIT_DELETED_BEFORE_COMMIT] = '%c{color:orange;font-weight:bold}delete-before-commit';

    function compare(a, b) {
        if (a > b) {
            return 1;
        }
        if (a < b) {
            return -1;
        }
        return 0;
    }

    function getFieldCount(dataset) {
        if (dataset.getRowCount() === 0) {
            return 0;
        }

        var row = dataset.getRow(0);
        var fields = row.getFieldNames();
        return fields.length * dataset.getRowCount();
    }

    // Sorting functions
    var SORT_FUNCS = {
        // by dataset name
        name: function sortName(a, b) {
            return compare(a.dataset.getName(), b.dataset.getName());
        },
        // by row count
        rows: function sortRow(a, b) {
            return compare(a.dataset.getRowCount(), b.dataset.getRowCount());
        },

        // by field count
        fields: function sortFields(a, b) {
            return compare(getFieldCount(a.dataset), getFieldCount(b.dataset));
        }
    };

    Toronto.color = {
        COLOR_RESET: 'color:black',
        _enabled: true,
        _colorIndex: 0,
        _brightnessIndex: 100,
        _done: {},
        _colorMap: {},

        // Color a value based on its type
        value: function(value) {
            if (!isNaN(value)) {
                return '%c{color:blue}' + value;
            }

            if (strictParseBoolean(value) !== null) {
                return '%c{color:red}' + value;
            }

            return '%c{color:darkgreen}"' + value + '"';
        },

        // Generate a Unique color for a UUID
        id: function(id, style) {
            if (!this._enabled) {
                return id;
            }

            if (typeof id === 'undefined') {
                return '%c{color:black}' + id;
            }

            if (typeof style === 'undefined') {
                style = [];
            }

            if (id instanceof Array) {
                var me = this;
                return id.map(function(o) {
                    return me._getColor(o, style);
                }).join(',');
            }

            return this._getColor(id, style);
        },

        // Get a unique color for a ID
        _getColor: function(id, style) {
            var color = this._colorMap[id];
            if (color === undefined) {
                color = this._getNextColor();
                this._colorMap[id] = color;
            }

            return '%c{color:' + color + ';' + style.join(';') + '}' + id;
        },

        // Generate a randomish new color.
        _getNextColor: function() {
            var color = 'hsl(' + this._colorIndex + ', ' + this._brightnessIndex + '%, 34%)';

            this._done[color] = true;
            this._colorIndex += 0.75 * 360;

            if (this._colorIndex > 360) {
                this._colorIndex -= 360;
                this._brightnessIndex -= 40;
                if (this._brightnessIndex < 60) {
                    this._brightnessIndex = 100;
                    this._colorIndex += 23;
                }
            }

            return color;
        },

        // convert between inline styles and chrome inspector style
        _makeStyle: function(args) {
            var output = [];
            var styles = [];
            var o;
            for (var j = 0; j < args.length; j++) {
                var arg = args[j];

                if (typeof arg !== 'string') {
                    arg = arg + '';
                }

                var data = arg.split('%c');
                // found %c
                if (data[0] !== '' || output.length === 0) {
                    output.push(data[0]);
                    if (data[0] !== '' && output.length > 1) {
                        styles.push('color:black;font-weight:normal');
                    }
                }
                for (var i = 1; i < data.length; i++) {
                    var obj = data[i].split('}');
                    var style = 'font-weight:normal;' + obj[0].substring(1);
                    var message = obj[1];

                    // console.log(style, styles[styles.length - 1]);
                    if (styles[styles.length - 1] === style) {
                        o = output[output.length - 1];
                        output[output.length - 1] = o + message;
                    } else {
                        output.push(message);
                        styles.push(style);
                    }
                }

                // console.log(styles[styles.length - 1]);
                if (styles[styles.length - 1] === this.COLOR_RESET) {
                    o = output[output.length - 1];
                    output[output.length - 1] = o + ' ';
                } else {
                    output.push(' ');
                    styles.push('color:black');
                }

            }
            // console.log([output.join('%c')].concat(styles));
            if (this._enabled) {
                return [output.join('%c')].concat(styles);
            } else {
                return [output.join('')];
            }
        },

        groupCollapsed: function() {
            var output = Toronto.color._makeStyle(arguments);
            console.groupCollapsed.apply(console, output);
        },

        group: function() {
            var output = Toronto.color._makeStyle(arguments);
            console.group.apply(console, output);
        },

        log: function() {
            var output = Toronto.color._makeStyle(arguments);
            console.log.apply(console, output);
        },

        getCommitActionColor: function(commitAction) {
            return COMMIT_COLOR[commitAction];
        },

    };

    Toronto.debug = {

        /**
         * Debug a datasets value
         *
         * eg:
         *  Toronto.debug.datasetValue('field.value');
         *  Toronto.debug.datasetValue('field.value', 5);
         *  Toronto.debug.datasetValue(datasetObject, 'value', 5);
         */
        datasetValue: function(searchString, rowIndex, optionalRowIndex) {
            var dataset, field, row;

            if (typeof searchString === 'string') {
                var parts = field.split('.');

                if (!isNaN(parts[0])) {
                    parts[0] = parseInt(parts[0], 10);
                }

                dataset = this.get(parts[0]);
                if (dataset === null) {
                    return;
                }
            }

            if (typeof searchString === 'object') {
                dataset = searchString;
                field = rowIndex;
                rowIndex = optionalRowIndex;
            }

            if (typeof rowIndex === 'undefined') {
                rowIndex = -1;
            }

            if (rowIndex === -1) {
                row = dataset.getCurrentRowIndex();
            } else {
                row = dataset.getRow(rowIndex);
            }

            if (row === null) {
                return console.log('No row ' + rowIndex + ' for dataset "' + dataset.getName() + '"');
            }

            return row.getField(field);
        },

        // Debug out a dataset
        //
        dataset: function(ds) {
            var dataset = this.get(ds);

            if (dataset === null) {
                this.log('Unable to find dataset "' + ds + '"');
                return;
            }

            if (dataset === undefined) {
                return;
            }

            this._logDatasetByObject(dataset);
        },

        /**
         * Get a dataset object
         *
         * dd() - list all datasets on the screen
         * dd(1) - debug the first dataset in the list
         * dd(/block/) - list all datasets that name contains 'block'
         * dd('block') - list all datasets with name 'block'
         * dd(function(dataset) { ... }) - list all datasets that pass the closure.
         */
        get: function(ds) {
            var dstype = typeof ds;
            var obj;

            // Was passed in a Dataset Object
            if (dstype === 'object' && typeof ds.getName === 'object') {
                return ds;
            }

            // Regexp was passed in
            if (dstype === 'object' && typeof ds.test === 'function') {
                this._scanDataContexts(ds);
            }

            if (dstype === 'undefined') {
                this._scanDataContexts();
            }

            // Get by index
            if (dstype === 'number') {
                return this._datasetByIndex[parseInt(ds, 10) - 1].dataset;
            }

            var i;
            var index = 0;
            // Find by Dataset Name or Dataset ID
            if (dstype === 'string') {
                for (i = 0; i < this._datasetByIndex.length; i++) {
                    obj = this._datasetByIndex[i];

                    if (obj.dataset.getName() === ds || obj.dataset.getID() === ds) {
                        this._logDataset(index, obj);
                        index++;
                    }
                }
                if (index > 0) {
                    return;
                }
                return null;
            }

            // Log them all out!
            for (i = 0; i < this._datasetByIndex.length; i++) {
                if (dstype === 'function') {
                    if (!ds(this._datasetByIndex[i].dataset) ){
                        continue;
                    }
                }
                this._logDataset(i, this._datasetByIndex[i]);
            }
        },

        _logDataset: function(index, obj) {
            Toronto.color.log(('   ' + (index + 1)).slice(-3) + ':',
                this._styleDatasetHeader(obj.dataset, obj.context));
        },

        _scanDataContexts: function(search, context, depth) {

            depth = depth || 0;

            if (typeof context === 'undefined') {
                context = Toronto.internal.GlobalState.getWidgetTree().getRootDataContext();
                this._datacontexts = {};
            }

            if (depth === 0){
                this._datasets = {};
                this._datasetByID = {};
                this._datasetByIndex = [];
                this._longestDatasetName = '';
                this._longestDatasetPadding = ' ';
            }

            var i;
            this._datacontexts[context.getID().toLowerCase()] = context;

            var datasets = context.getAllDatasets();
            for (i = 0; i < datasets.length; i++) {
                var ds = datasets[i];
                var obj = {
                    'context': context.getID(),
                    'dataset': ds
                };
                var dsName = ds.getName();

                // This is no the dataset we are looking for!
                if (search !== undefined) {
                    if (!search.test(dsName)) {
                        continue;
                    }
                }

                this._datasetByIndex.push(obj);
                if (this._datasets[dsName] !== undefined) {
                    this._datasets[dsName].push(obj);
                } else {
                    this._datasets[dsName] = [obj];
                }

                if (dsName.length > this._longestDatasetName.length) {
                    for (var j = this._longestDatasetName.length; j < dsName.length; j++) {
                        this._longestDatasetPadding += ' ';
                    }

                    this._longestDatasetName = dsName;
                }
                this._datasetByID[ds.getID()] = obj;
            }

            // recurse into all child objects
            var children = context.getAllChildContexts();
            for (i = 0; i < children.length; i++) {
                this._scanDataContexts(search, children[i], depth + 1);
            }
            // Only sort when we are back at the top
            if (depth === 0) {
                if (typeof this.SORT_FUNC === 'function') {
                    datasets = this._datasetByIndex.sort(this.SORT_FUNC);
                }
            }
        },


        _styleDatasetHeader: function(dataset, contextID) {
            var name = (dataset.getName() + this._longestDatasetPadding).slice(0, this._longestDatasetName.length);

            var parent = dataset.getParentDataset();
            var parentString = '';
            if (parent !== null) {
                parentString = '\t\t%c{color:black}parent:' + Toronto.color.id(parent.getID());
            }

            var contextString = '';
            if (typeof contextID !== 'undefined') {
                contextString = '\t%c{color:black}context:' + Toronto.color.id(contextID);
            }

            // style the name red if the dataset is invalid
            if (!dataset.getMetadata().isValid()) {
                name = '%c{color:red}' + name;
            }

            var fieldCount = getFieldCount(dataset);

            return name +
                '\t%c{color:black}id:' + Toronto.color.id(dataset.getID()) +
                '\t\t%c{color:black}rows:' + Toronto.color.value(dataset.getRowCount()) + '    '.slice(0, 4 - ('' + dataset.getRowCount()).length) +
                '\t\t%c{color:black}fields:' + Toronto.color.value(fieldCount) + '    '.slice(0, 6 - ('' + fieldCount).length) +
                contextString +
                parentString;

        },

        _logDatasetByObject: function(ds) {
            var rows = ds.getAllRows();

            Toronto.color.group('%c{color:black}name:' + this._styleDatasetHeader(ds));

            for (var i = 0; i < rows.length; i++) {
                var r = rows[i];
                var rowID = Toronto.color.id(r.getRowID());

                var parentID = getParentRowID(r);
                var parentString = '';
                if (parentID !== undefined && parentID !== null) {
                    parentString = '\t%c{color:black}parent:' + Toronto.color.id(parentID);
                }


                var currentRow = r.isCurrentRow() ? '  %c{color:green}>%c{color:black} ' : '    ';
                var commit = COMMIT_COLOR[r.getCommitAction()];
                var commitString = '';
                if (r.getCommitAction() !== 'none') {
                    commitString = '\t%c{color:black}commitAction:' + commit;
                }


                var fields = r.getFieldNames();
                var output = [];
                // console.group(rowID, 'color:red');
                for (var f = 0; f < fields.length; f++) {
                    var field = fields[f];
                    var isValid = r.getFieldObject(field).getMetadata().isValid();

                    var validColor = '%c{color:black}';
                    if (!r.getFieldObject(field).getMetadata().isValid()) {
                        validColor = '%c{color:red}';
                    }

                    output.push([validColor + field + ':\t' + Toronto.color.value(r.getField(field))]);
                }

                var invalidColor = '';
                if (!r.getMetadata().isValid()) {
                    invalidColor = '%c{color:red}';
                }

                var rowString = currentRow + invalidColor + i + '\t%c{color:black}id:' + rowID +
                    parentString +
                    commitString;

                var outputString = output.join(', ');
                if (outputString.length < 500) {
                    Toronto.color.log(rowString + '\t%c{color:black}fields:[ ' + outputString + '%c{color:black}]');
                } else {
                    Toronto.color.groupCollapsed(rowString);
                    for (var o = 0; o < output.length; o++) {
                        Toronto.color.log(output[o]);
                    }
                    console.groupEnd();
                }

            }
            console.groupEnd();

            return ds;

        },


        setSort: function(sort) {
            this._datasetByIndex = undefined;
            if (typeof sort === 'function') {
                this.SORT_FUNC = sort;
                return;
            }

            if (typeof sort === 'string') {
                console.log('setting sort function to "' + sort + '"');
                this.SORT_FUNC = SORT_FUNCS[sort.toLowerCase()];
                return;
            }

            console.log('unsetting sort function');
            Toronto.debug.SORT_FUNC = undefined;

        }
    };

    var getParentRowID = function(row) {
        if (typeof row.getParentRow !== 'undefined') {
            var parentRow = row.getParentRow();
            if (parentRow !== null) {
                return parentRow.getRowID();
            }
        }
        if (typeof row.getParentRowID !== 'undefined') {
            return row.getParentRowID();
        }
    };

    // Get Dataset
    window.gd = function(ds) {
        return Toronto.debug.get(ds);
    };

    // Debug Datasets
    window.dd = function(ds) {
        Toronto.debug.dataset(ds);
    };

    // Debug Dataset Sort
    window.dds = function(sort) {
        Toronto.debug.setSort(sort);
    };

})();